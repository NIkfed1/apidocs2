---
title: QIWIPay Reference

metatitle: QIWIPay Reference

metadescription: QIWIPay interface is intended for card payment operations. The service allows RSP to accept secure payments from their clients.

language_tabs:
  - json
  - php
  - html
  - shell

toc_footers:
  - <a href='/'>Home page</a>

includes:

search: true
---

 *[WPF]: Web Payment Form - a payment form to enter card data for an item.
 *[3DS]: 3-D Secure - a messaging protocol to exchange data during e-commerce transaction for consumer authentication purposes.
 *[OCT]: Original Credit Transaction - a financial transaction that credits the Cardholder’s account.
 *[API]: Application Programming Interface -  a set of ready-for-use methods that allow the creation of applications which access the features or data of an application or other service.
 *[HMAC]: Hash-based message authentication code - a type of message authentication code involving a cryptographic hash function.
 *[RSP]: Retail Service Provider
 *[MPI]: Merchant Plug-In - a module integrated into QIWIPay store-front applications used to process authentication transactions.
 *[PCI DSS]: Payment Card Industry Data Security Standard -  The Payment Card Industry Data Security Standard – a proprietary information security standard for storing, processing and transmitting credit card data.
 *[REST]: Representational State Transfer -  a software architectural pattern for Network Interaction between distributed application components.
 *[JSON]: JavaScript Object Notation - a lightweight data-interchange format based on JavaScript.
 *[Luhn]: Luhn Algorithm - a checksum formula used for verifying and validating identification numbers against accidental errors.


# Introduction

QIWIPay interface is intended for card payment operations. The service allows RSP to accept secure payments from their clients.

QIWIPay supports the following: payment processing, a two-step payment confirmation, refunds, recurring payments, recurring payments cancellation, transaction status query.

# Integration Methods

You can use one of the following integration methods on QIWIPay:

* QIWIPay WPF - a web payment form on QIWI side. Quick and easy solution for accepting payments with limited functions (only payment transactions). Web-form address: https://pay.qiwi.com/payform.
* QIWIPay API - fully functional REST-based API for making card payments. All requests are POST-type and the parameters are in JSON body. RSP implements only the Payment Form Interface. Access API on https://acquiring.qiwi.com.

<aside class="warning">
Due to Card Data Processing, QIWIPay API can be used by PCI DSS-certified merchants only .
</aside>


# Operations {#opcode}

To indicate which operation is performed, use [`Operation code`](#section-2) (Op.Code) in each request. Operation code should be in each RSP request to API or to WPF form payload.

The following operations are accessible through QIWIPay.

Op.Code | QIWIPay WPF | QIWIPay API | Operation | Description
------------ | ----------- | ----------- | -------- | --------
1 | + | + | sale | One-step payment
2 | - | + | finish_3ds | Return to web-site after 3DS authentication
3 | + | + | auth | Auth request for two-step payments
5 | - | + | capture | Confirm auth for two-step payment
6 | - | + | reversal | Cancel payment
7 | - | + | refund | Refund payment
8 | - | + | refund_cancel | Cancel refund
10 | + | + | recurring_init_sale | Initial payment for recurring payment (one-step payment)
11 | + | + | recurring_init_auth | Initial payment for recurring payment (two-step payment)
12 | - | + | recurring | Recurring payment
13 | - | + | recurring_cancel | Cancel recurring payments
20 | - | + | payout | Payout operation (OCT)
30 | - | + | status | Operation status query

# Operation Methods

## QIWIPay Flows

Two payment flows can be used in QIWIPay:

* One-step - `sale` operation
* Two-step - `auth` operation -> `capture` operation

Similarly, two flows to initiate recurring payments can be used:

* One-step - `recurring_init_sale` operation
* Two-step - `recurring_init_auth` operation -> `capture` operation

<aside class="notice">
Only the first operation of the two-step flow  (<b>auth</b> or <b>recurring_init_auth</b>) is used in QIWIPay WPF.
</aside>

## 3DS

3DS (3-D Secure) is a common notation for technological frameworks of  Visa and MasterCard (Verified By Visa and MasterCard Secure Code, correspondingly). The main goal is to verify the Cardholder's identity by the Issuer before a payment and to prevent card data theft. In practice it work like that. The Cardholder enters his/her card details. The Issuer’s secure web page pops up requesting for verification data (a password or a secret code) to be sent by the Cardholder via SMS. If verification data is valid the payment is processed, if not valid it is rejected.

`sale` operation via QIWIPay can be 3DS-validated if the Issuer requires 3DS authentication.

QIWIPay WPF Payment flow with 3DS verification is explained on the following scheme.

<img src="/images/3ds_en.png" class="image" />


# QIWIPay WPF

RSP only redirects customers to https://pay.qiwi.com/payform. Then customers complete the payment form on QIWIPay web-site with their card details. 

## Payment Flow

QIWIPay WPF one-step payment flow is explained on the following scheme.

<img src="/images/wpf_sale_en.png" class="image" />


## Redirect to QIWIPay WPF

To redirect, use standard web form HTTP POST data request.

~~~ html
<form method="post" action="https://pay.qiwi.com/paypage/initial">
  <input name="opcode" type="hidden" value="1">
  <input name="merchant_site" type="hidden" value="99">
  <input name="currency" type="hidden" value="643">
  <input name="sign" type="hidden" value="bb5c48ea540035e6b7c03c8184f74f09d26e9286a9b8f34b236b1bf2587e4268">
  <input name="amount" type="text">
  <input type="submit" value="Pay">
</form>
~~~

No authorization is required for RSP.

Use the following form fields for payment operations. All fields not specified by the customer should be with `hidden` type.

Parameter|Required|Data type|Description
--------|-------|----------|--------
opcode|Yes|integer|Op.Code (1, 3, 10, 11 only)
merchant_site|Yes|integer|RSP site ID (issued by QIWI)
currency|Yes|integer|Operation currency by ISO 4217
[sign](#sign)|Yes|string(64)|Checksum of the request's parameters
amount|No|decimal|Operation amount
order_id|No|string(256)|Unique order ID assigned by RSP system
email|No|string(64)|Customer E-mail
country|No|string(3)|Customer Country by ISO 3166-1
city|No|string(64)|Customer City of residence
region|No|string(6)|Customer Region (geo code) by ISO 3166-2
address|No|string(64)|Customer Address of residence
phone|No|string(15)|Customer contact phone number
cf1|No|string(256)|Arbitrary information about the operation, such as list of services
cf2|No|string(256)|Arbitrary information about the operation, such as list of services
cf3|No|string(256)|Arbitrary information about the operation, such as list of services
cf4|No|string(256)|Arbitrary information about the operation, such as list of services
cf5|No|string(256)|Arbitrary information about the operation, such as list of services
cf5|No|string(256)|Arbitrary information about the operation, such as list of services
product_name|No|string(256)|Service description.
merchant_uid|No|string(64)|Unique Customer ID assigned by RSP system
order_expire|No|YYYY-MM-DDThh:mm:ss±hh:mm|Order expiration date by ISO8601 with time zone
callback_url|No|string(256)|Callback URL
success_url|No|string(256)|Redirect URL when payment is successful
decline_url|No|string(256)|Redirect URL when payment is unsuccessful


# QIWIPay API

## Authorization

QIWIPay API authorizes the request by RSP client certificate validation. Certificate is issued by QIWI for each RSP.

<aside class="notice">
To obtain certificate, please contact QIWI Support.
</aside>

## Payment Flow

### One-step payment flow

<img src="/images/api_sale_en.png" class="image" />

### Two-step payment flow

<img src="/images/api_auth_en.png" class="image" />

## Sale Operation {#sale}

### Request

~~~ json
{
  "opcode": 1,
  "merchant_uid": "10001",
  "merchant_site": 99,
  "pan": "4111111111111111",
  "expiry": "1219",
  "cvv2": "123",
  "amount": 4678.5,
  "currency": 643,
  "card_name": "cardholder name",
  "order_id": "order1231231",
  "ip": "127.0.0.1",
  "email": "merchant@qiwi.com",
  "country": "RUS",
  "city": "Moscow",
  "region": "606008",
  "address": "South Park",
  "phone": "79166554321",
  "user_device_id": "12312321Un43243",
  "user_timedate": "26.08.2016",
  "user_screen_res": 1280,
  "user_agent": "Mozilla FF 312",
  "cf1": "cf1",
  "cf2": "cf2",
  "cf3": "cf3",
  "cf4": "cf4",
  "cf5": "cf5",
  "callback_url": "http://url",
  "success_url": "http://url",
  "decline_url": "http://url",
  "product_name": "Flowers",
  "sign": "bb5c48ea540035e6b7c03c8184f74f09d26e9286a9b8f34b236b1bf2587e4268"
}
~~~

Parameter|Required|Data type|Description
--------|-------|----------|--------
opcode|Yes|integer|Op.Code (1)
merchant_site|Yes|integer|RSP site ID (issued by QIWI)
pan|Yes|string(19)|Card number (PAN)
expiry|Yes|string(4)|Card expiry date
cvv2|Yes|string(4)|CVV2/CVC2 code
amount|Yes|decimal|Operation amount
currency|Yes|integer|Operation currency by ISO 4217
[sign](#sign)|Yes|string(64)|Checksum of the request's parameters
card_name|Yes, if available|string(64)|Cardholder name as printed on the card (Latin letters)
order_id|Yes, if available|string(256)|Unique order ID assigned by RSP system
ip|Yes, if available|string(15)|Customer IP address
email|Yes, if available|string(64)|Customer E-mail
country|Yes, if available|string(3)|Customer Country by ISO 3166-1
user_device_id|Yes, if available|string(64)|Customer Unique device ID
city|Yes, if available|string(64)|Customer City of residence
region|Yes, if available|string(6)|Customer Region (geo code) by ISO 3166-2
address|Yes, if available|string(64)|Customer Address of residence
phone|Yes, if available|string(15)|Customer contact phone number
user_timedate|No|string(64)|Local time in Customer browser
user_screen_res|No|string(64)|Screen resolution of the Customer's display
user_agent|No|string(256)|Customer browser
cf1|No|string(256)|Arbitrary information about the operation, such as list of services
cf2|No|string(256)|Arbitrary information about the operation, such as list of services
cf3|No|string(256)|Arbitrary information about the operation, such as list of services
cf4|No|string(256)|Arbitrary information about the operation, such as list of services
cf5|No|string(256)|Arbitrary information about the operation, such as list of services
product_name|No|string(256)|Service description.
merchant_uid|No|string(64)|Unique Customer ID assigned by RSP system
order_expire|No|YYYY-MM-DDThh24:mm:ss|Order expiration date by ISO8601
callback_url|No|string(256)|Callback URL
success_url|No|string(256)|Redirect URL when payment is successful
decline_url|No|string(256)|Redirect URL when payment is unsuccessful

### Response for card with no 3DS

~~~ json
{
  "txn_id":172001,
  "txn_status":60,
  "txn_type":1,
  "error_code":0,
  "pan": "411111xxxxxx1111",
  "amount": 4678.5,
  "currency": 643,
  "auth_code": "2G4923",
  "eci":
}
~~~

Parameter|Data type|Description
--------|----------|--------
txn_id | integer | Transaction ID
txn_status | integer | [Transaction status](#txn_status)
txn_type | integer | [Transaction type](#txn_type)
error_code | integer | [System error code](#errors)
pan | string(19) | Customer Card number (PAN)
amount | decimal | Amount to debit from card account
currency | integer | Operation currency by ISO 4217
auth_code | string(6) | Authorization code
eci | string(2) | Electronic Commerce Indicator

### Response for card with 3DS

~~~ json
{
  "txn_id":172001,
  "txn_status":40,
  "txn_type":1,
  "error_code":0,
  "acs_url":"https://test.paymentgate.ru/acs/auth/start.do",
  "pareq":"eJxVUmtvgjAUuG79oClYe51uDcsi2B+ZrJPgKtipGHRnUTgy1w5bVwyf4BfjpuJ....."
}
~~~

Parameter|Data type|Description
--------|----------|--------
txn_id | integer | Transaction ID
txn_status | integer | [Transaction status](#txn_status)
txn_type | integer | [Transaction type](#txn_type)
error_code | integer | [System error code](#errors)
acs_url | string(1024) | Issuing Bank URL where to redirect Customer
pareq | string(4096) | Unique Customer ID to use for `acs_url` redirect.

You need to redirect Customer to 3DS standard redirection URL.

## Finish 3DS authentication

When Customer returns from 3DS standard redirection URL and completes 3DS authentication successfully, you need to send the following request to finish 3DS authentication process.

### Request after successful 3DS authentication

~~~ json
{
  "opcode":2,
  "merchant_site":99,
  "pares": "fdsadfsd",
  "txn_id": "172001"
}
~~~

Parameter|Required|Data type|Description
--------|-------|----------|--------
opcode | Yes | integer | Op.Code (2)
merchant_site | Yes | integer | RSP site ID (issued by QIWI)
pares | Yes | string(4096) | Customer verification result
txn_id | Yes | integer | Transaction ID
[sign](#sign) | Yes | string(64) | Checksum of the request's parameters

### Response to 3DS authentication result

~~~ json
{
  "txn_id":172001,
  "txn_status":60,
  "txn_type":1,
  "error_code":0,
  "pan": "411111xxxxxx1111",
  "amount": 4678.5,
  "currency": 643,
  "auth_code": "2G4923",
  "eci": "05"
}
~~~

Parameter|Data type|Description
--------|----------|--------
txn_id | integer | Transaction ID
txn_status | integer | [Transaction status](#txn_status)
txn_type | integer | [Transaction type](#txn_type)
error_code | integer | [System error code](#errors)
pan | string(19) | Customer Card number (PAN)
amount | decimal | Amount to debit from card account
currency | integer | Operation currency by ISO 4217
auth_code | string(6) | Authorization code
eci | string(2) | Electronic Commerce Indicator

## Sale Confirmation Operation

### Request

~~~ json
{
  "opcode": 5,
  "merchant_site": 99,
  "txn_id": "172001",
  "sign": "bb5c48ea540035e6b7c03c8184f74f09d26e9286a9b8f34b236b1bf2587e4268"
}
~~~

Parameter|Required|Data type|Description
--------|-------|----------|--------
opcode|Yes|integer|Op.Code (5)
merchant_site|Yes|integer|RSP site ID (issued by QIWI)
txn_id | Yes | integer | Transaction ID
[sign](#sign) | Yes | string(64) | Checksum of the request's parameters

### Response

~~~ json
{
  "txn_id":172001,
  "txn_status":60,
  "txn_type":1,
  "error_code":0
}
~~~

Parameter|Data type|Description
--------|----------|--------
txn_id | integer | Transaction ID
txn_status | integer | [Transaction status](#txn_status)
txn_type | integer | [Transaction type](#txn_type)
error_code | integer | [System error code](#errors)

## Cancel Operation

### Request

~~~ json
{
  "opcode":6,
  "merchant_site": 99,
  "txn_id": 181001,
  "amount": 700,
  "sign": "bb5c48ea540035e6b7c03c8184f74f09d26e9286a9b8f34b236b1bf2587e4268"
}
~~~

Parameter|Required|Data type|Description
--------|-------|----------|--------
opcode|Yes|integer|Op.Code (6)
merchant_site|Yes|integer|RSP site ID (issued by QIWI)
txn_id | Yes | integer | Transaction ID
amount|No|decimal|Operation amount
[sign](#sign) | Yes | string(64) | Checksum of the request's parameters

### Response

~~~ json
{
  "txn_id":182001,
  "txn_status":72,
  "txn_type":4,
  "error_code":0,
  "amount": 700
}
~~~

Parameter|Data type|Description
--------|----------|--------
txn_id | integer | Transaction ID
txn_status | integer | [Transaction status](#txn_status)
txn_type | integer | [Transaction type](#txn_type)
error_code | integer | [System error code](#errors)
amount | decimal | Amount to debit

## Refund Operation

### Request

~~~ json
{
  "opcode":7,
  "merchant_site": 99,
  "txn_id": 181001,
  "amount": 700,
  "sign": "bb5c48ea540035e6b7c03c8184f74f09d26e9286a9b8f34b236b1bf2587e4268"
}
~~~

Parameter|Required|Data type|Description
--------|-------|----------|--------
opcode|Yes|integer|Op.Code (7)
merchant_site|Yes|integer|RSP site ID (issued by QIWI)
txn_id | Yes | integer | Transaction ID
amount|No|decimal|Operation amount
[sign](#sign) | Yes | string(64) | Checksum of the request's parameters

### Response

~~~ json
{
  "txn_id":182001,
  "txn_status":77,
  "txn_type":3,
  "error_code":0,
  "amount": 700
}
~~~

Parameter|Data type|Description
--------|----------|--------
txn_id | integer | Transaction ID
txn_status | integer | [Transaction status](#txn_status)
txn_type | integer | [Transaction type](#txn_type)
error_code | integer | [System error code](#errors)
amount | decimal | Amount to debit

## Recurring Payment Operation

### Request

~~~ json
{
  "opcode": 12,
  "merchant_uid": "10001",
  "merchant_site": 99,
  "txn_id": 172001,
  "amount": 4678.5,
  "currency": 643,
  "order_id": "order1231231",
  "cf1": "cf1",
  "cf2": "cf2",
  "cf3": "cf3",
  "cf4": "cf4",
  "cf5": "cf5",
  "product_name": "Название услуги",
  "sign": "bb5c48ea540035e6b7c03c8184f74f09d26e9286a9b8f34b236b1bf2587e4268"
}
~~~

Parameter|Required|Data type|Description
--------|-------|----------|--------
opcode|Yes|integer|Op.Code (12)
merchant_site|Yes|integer|RSP site ID (issued by QIWI)
txn_id | Yes | integer | Transaction ID
amount|Yes|decimal|Operation amount
[sign](#sign)|Yes|string(64)|Checksum of the request's parameters
order_id|Yes, if available|string(256)|Уникальный номер заказа в системе ТСП
cf1|No|string(256)|Arbitrary information about the operation, such as list of services
cf2|No|string(256)|Arbitrary information about the operation, such as list of services
cf3|No|string(256)|Arbitrary information about the operation, such as list of services
cf4|No|string(256)|Arbitrary information about the operation, such as list of services
cf5|No|string(256)|Arbitrary information about the operation, such as list of services
product_name|No|string(256)|Service description.

### Response

~~~ json
{
  "txn_id":182001,
  "txn_status":60,
  "txn_type":5,
  "error_code":0,
  "amount": 4678.5,
  "currency": 643,
  "auth_code": "2G4923"
}
~~~

Parameter|Data type|Description
--------|----------|--------
txn_id | integer | Transaction ID
txn_status | integer | [Transaction status](#txn_status)
txn_type | integer | [Transaction type](#txn_type)
error_code | integer | [System error code](#errors)
amount | decimal | Amount to debit
currency | integer | Operation currency by ISO 4217
auth_code | string(6) | Authorization code

## Status Query Operation

### Request

~~~ json
{
  "opcode":30,
  "merchant_site": 99,
  "order_id": "41324123412342",
  "sign": "bb5c48ea540035e6b7c03c8184f74f09d26e9286a9b8f34b236b1bf2587e4268"
}
~~~

Parameter|Required|Data type|Description
--------|-------|----------|--------
opcode|Yes|integer|Op.Code (30)
merchant_site|Yes|integer|RSP site ID (issued by QIWI)
txn_id | Yes | integer | Transaction ID
[sign](#sign)|Yes|string(64)|Checksum of the request's parameters
order_id|No|string(256)|Уникальный номер заказа в системе ТСП

### Response

~~~ json
{
  "transactions": [
    {
      "error_code": 0,
      "txn_id": 3666050,
      "txn_status": 52,
      "txn_type": 2,
      "pan": "400000******0002",
      "amount": 10000,
      "currency": 643,
      "auth_code": "181218",
      "merchant_site": 99,
      "card_name": "cardholder name",
      "card_bank": "",
      "order_id": "41324123412342"
    },
    {
      "error_code": 0,
      "txn_id": 3684050,
      "txn_status": 72,
      "txn_type": 4,
      "pan": "400000******0002",
      "amount": 100,
      "currency": 643,
      "merchant_site": 99,
      "card_name": "cardholder name",
      "card_bank": ""
    },
    {
      "error_code": 0,
      "txn_id": 3685050,
      "txn_status": 72,
      "txn_type": 4,
      "pan": "400000******0002",
      "amount": 100,
      "currency": 643,
      "merchant_site": 99,
      "card_name": "cardholder name",
      "card_bank": ""
    }
  ],
  "error_code": 0
}
~~~

<aside class="warning">
Response body contains JSON Array
</aside>

Parameter|Data type|Description
--------|-------|----------|--------
txn_id | integer | Transaction ID
txn_status | integer | [Transaction status](#txn_status)
txn_type | integer | [Transaction type](#txn_type)
error_code | integer | [System error code](#errors)
pan|string(19)|Masked Card number (PAN), first six and last four digits are unmasked
amount|decimal|Amount to debit
currency|integer|Operation currency by ISO 4217
auth_code | string(6) | Authorization code
eci | string(2) | Electronic Commerce Indicator
card_name|string(64)|Cardholder name as printed on the card (Latin letters)
card_bank|string(64)|Issuing Bank
order_id|string(256)|Unique order ID assigned by RSP system
ip|string(15)|Customer IP address
email|string(64)|Customer E-mail
country|string(3)|Customer Country by ISO 3166-1
city|string(64)|Customer City of residence
region|string(6)|Customer Region (geo code) by ISO 3166-2
address|string(64)|Customer Address of residence
phone|string(15)|Customer contact phone number
cf1|No|string(256)|Arbitrary information about the operation, such as list of services
cf2|No|string(256)|Arbitrary information about the operation, such as list of services
cf3|No|string(256)|Arbitrary information about the operation, such as list of services
cf4|No|string(256)|Arbitrary information about the operation, such as list of services
cf5|No|string(256)|Arbitrary information about the operation, such as list of services
product_name|string(256)|Service description.

# Signature of the Request {#sign}

~~~ php
<?php
$hmac = hash_hmac('sha256','param1_value|param2_value|param3_value','secret_key');
?>

Output:
bb5c48ea540035e6b7c03c8184f74f09d26e9286a9b8f34b236b1bf2587e4268
~~~

~~~ shell
user@server:~$ echo -n "param1_value|param2_value|param3_value" | openssl dgst -sha256  -hmac "secret_key"
(stdin)= bb5c48ea540035e6b7c03c8184f74f09d26e9286a9b8f34b236b1bf2587e4268
~~~

You should sign all parameters in each operation request. Add the signature in `sign` parameter for each request. Signature is calculated as HMAC algorithm with SHA256-hash function.

* Parameters are in alphabetical order.
* Separator is `|`.
* Signed are all the parameters in the request.

# Payment Notification {#callback}

There are two types of notification flow:

* During payment processing, when card debit is confirmed and before showing status page or RSP response (depends on what flow is used, WPF or API).
* On background, when payment processed.

To guarantee QIWI origin of the notifications, accept only these IP addresses related to QIWI:

* 91.232.231.36
* 79.142.22.81
* 79.142.28.154

Notification goes to `callback_url` parameter from the request, as a POST request with the following parameters:

Parameter|Datat type| Used for signature | Description
--------|----------|------------------|---------
txn_id | integer | + | Transaction ID
txn_status | integer | + | [Transaction status](#txn_status)
txn_type | integer | + | [Transaction type](#txn_type)
error_code | integer | + | [System error code](#errors)
pan | string(19) | - | Customer Card number (PAN), first six and last four digits are unmasked
amount | decimal | + | Amount to debit
currency | integer | + | Operation currency by ISO 4217
auth_code | string(6) | - | Authorization code
eci | string(2) | - | Electronic Commerce Indicator
card_name | string(64) | - | Cardholder name as printed on the card (Latin letters)
card_bank | string(64) | - | Issuing Bank
order_id | string(256) | - | Unique order ID assigned by RSP system
ip | string(15) | + | Customer IP address
email | string(64) | + | Customer E-mail
country | string(3) | - | Customer Country by ISO 3166-1
city | string(64) | - | Customer City of residence
region | string(6) | - | Customer Region (geo code) by ISO 3166-2
address | string(64) | - | Customer Address of residence
phone | string(15) | - | Customer contact phone number
cf1 | string(256) | - | Arbitrary information about the operation, such as list of services
cf2 | string(256) | - | Arbitrary information about the operation, such as list of services
cf3 | string(256) | - | Arbitrary information about the operation, such as list of services
cf4 | string(256) | - | Arbitrary information about the operation, such as list of services
cf5 | string(256) | - | Arbitrary information about the operation, such as list of services
product_name | string(256) | - | Service description
sign | string(64) | - | Checksum (signature) of the request

RSP should verify request signature (`sign` parameter) to minimize possible fraud notifications.  

<aside class="notice">
Signature algorithm coincides with ordinary QIWIPay requests, though only the parameters marked above with a plus sign in <i>Used for signature</i> column are taken.
</aside>

Notification is treated as successfully delivered when RSP server responds with HTTP code `200 OK`.
So far, during the first 24 hours after the operation, the system will continue to deliver notification with incremental time intervals until RSP server responds with HTTP code `200 OK`.

<aside class="success">
Due to any reason, if RSP accepts notification without HTTP code <i>200 OK</i> then repeated notification should not change the status of the operation.
</aside>

<aside class="warning">
RSP takes all responsibilities for possible financial loss when <i>sign</i> is not verified for the illegal notifications.
</aside>

# Transaction Types {#txn_type}

Transaction types returned in `txn_type` parameter are in the following table.

Type | Name | Description
--- | -------- | --------
1 | Purchase | One-step payment
2 | Purchase | Authorization for two-step payment
6 | Purchase | One-step  operation for initial recurring payment
7 | Purchase | Authorization for two-step initial recurring payment
4 | Reversal | Cancel operation
3 | Refund | Refund operation
5 | Recurring | Recurring payment operation
0 | Unknown | Unknown operation type

# Transaction Statuses {#txn_status}

Status | Description | Possible operations
------ | -------- | -------- | ------------------
10 | Init | Base verification is passed and operation begins to process | -
40 | Awaiting3DS | Operation awaits one-time password from the Customer (3DS) | finish_3ds
52 | Authorized | Authorization is passed (hold) | capture, reversal
55 | Captured (Complited) | Confirmed operation | refund
60 | Reconciled | Completely finished financial operation | refund
65 | Settled | RSP is paid for this operation | refund
72 | Reversaled | Current sale operation completely cancelled | -
75 | Refund | Refund operation | refund_cancel
77 | Refund Reconciled | Completely finished financial refund operation | -
80 | Refund Settled | RSP is charged for this operation | -
150 | Declined | Operation declined | -

<aside class="notice">
If interaction with Acquiring bank is online, then status <i>55</i> is returned in case of <b>sale</b>, <b>capture</b> operations, while status 60 is returned in case of actual sending financial information.
</aside>

# Errors Description {#errors}

~~~json
Request
{
    "opcode": 1,
    "pan": "4",
    "expiry": "1010",
    "cvv2": "1",
    "amount": 4678.5,
    "card_name": "cardholder name",
    "merchant_site": 1000,
    "sign": "9FD874BF9D4483854E25D1D3371D01821AA4814CDFA4FE2033B5EDD4D5DE94C9"
}

Response
{
  "errors": [
    {
      "field": "pan",
      "message": "length of [pan] cannot be less than 13"
    },
    {
      "field": "expiry",
      "message": "card expired"
    },
    {
      "field": "cvv2",
      "message": "length of [cvv2] cannot be less than 3"
    }
  ],
  "error_message": "Validation errors",
  "error_code": 8024
}


Request
{

  "opcode" : 6,
  "merchant_site" : "1234",
  "txn_id" : "",
  "sign" : "sadads",
  "amount" : "1000.01"
}

Response
{
  "error_message":"Parsing error",
  "error_code":8006
}

~~~

Error code | Name | Description
---------- | -------- | --------
0 | No errors | No errors
8001 | Internal error | Acquiring Bank technical error
8002 | Operation not supported | Operation not supported
8004 | Temporary error | Server is busy, repeat later
8005 | Route not found | Route for transaction processing not found
8006 | Parsing error | Unable to process input request, incorrect format
8010 | Amount too big | `reversal`, `refund` operations amount exceeded
8011 | Card not supported |
8013 | Incorrect card expiration date |
8015 | Incorrect amount |
8016 | Merchant site not found | Unknown merchant with merchant_site_id
8017 | Order id is required | Incorrect order_id for status query
8018 | Transaction not found | Transaction not found
8019 | Incorrect opcode |
8023 | Transaction expired |
8024 | Validation errors | Input parameters validation errors. Details are in returned `errors` array.
8025 | Opcode is not allowed |
8026 | Incorrect parent transaction |
8027 | Incorrect parent transaction |
8028 | Card expired |
8033 | Transaction rejected |
8034 | Internal error |
8035 | Internal error |
8052 | Incorrect transaction state | Incorrect transaction status, attempt for `capture` after `finish_3ds` or `reversal`
8053 | Authentication failed |
8054 | Invalid signature |
8055 | Order already paid |
8056 | In process |
8057 | Card locked |
8058 | Access denied |
8059 | Currency is not allowed |
8061 | Transaction rejected | Issuer response: Payment rejected. Address to Issuing Bank.
8062 | Transaction rejected | Issuer response: Payment rejected. Try again
8063 | Transaction rejected | Issuer response: Payment rejected. Contact QIWI Support
8064 | Transaction rejected | Issuer response: Payment rejected. Limit exceeded. Address to Issuing Bank
8065 | Transaction rejected | Issuer response: Payment rejected. Check payment details
8066 | Transaction rejected | Issuer response: Transaction forbidden. Address to Issuing Bank
8666 | Internal error | Internal service error

# Test Data

To make tests for payment operations, you may use any card number corresponded to Luhn algorithm.

To make tests of various payment methods and responses, use different expiry dates:

* If month of expiry date is 02, then operation is treated as unsuccessful.
* If month of expiry date is 03, then operation will process successfully with 3 seconds timeout.
* If month of expiry date is 04, then operation will process unsuccessfully with 3 seconds timeout.
* In all other cases, operation is treated as successful.
