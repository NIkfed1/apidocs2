---
title: QIWI Wallet API

search: true

metatitle: API QIWI Кошелька

metadescription: API QIWI Кошелька позволяет автоматизировать получение информации о счёте и проводить операции с его помощью.

language_tabs:
  - shell
  - http: HTTP
  - json: JSON

toc_footers:
 - <a href='/'>На главную</a>
 - <a href='mailto:api_help@qiwi.com'>Обратная связь</a>
---

# Введение

API QIWI Кошелька позволяет автоматизировать получение информации о счёте и проводить операции с его помощью.

Методы API доступны после регистрации пользователя в [сервисе Visa QIWI Кошелек](https://qiwi.com). 

# Авторизация

Авторизация выполняется по стандарту CAS. См. <https://apereo.github.io/cas/5.0.x/protocol/CAS-Protocol-Specification.html>.

## Этап 1. tgt-ticket {#tgts}

TGT (ticket-granting ticket) выпускается при успешной авторизации клиента сервером CAS. Данный тикет используется для получения разрешений на операции в кошельке.

~~~shell
user@server:~$ сurl "https://auth.qiwi.com/cas/tgts"
  -X POST -v 
  -d '{ "login":"<login>","password":"<password>"}' 
  --header "Accept: application/vnd.qiwi.sso-v1+json" 
  --header "Content-Type: application/json" 
~~~

~~~http
POST /cas/tgts HTTP/1.1
Accept: application/vnd.qiwi.sso-v1+json
Content-Type: application/json
Host: auth.qiwi.com
User-Agent: ****

{ "login":"<wallet_login>","password":"<wallet_password>"}
~~~

Тип запроса - POST. 

URL запроса:

`https://auth.qiwi.com/cas/tgts`

Специальные заголовки запроса:

* `Accept: application/vnd.qiwi.sso-v1+json`
* `Content-Type: application/json`

Тело запроса должно быть в формате JSON. Все перечисленные параметры обязательны: 

Параметр|Тип|Описание
--------|----|----
login | String |Номер QIWI Кошелька (с международным кодом `7`)
password|String| Пароль от QIWI Кошелька

TGT содержится в поле `ticket` JSON-ответа.

~~~json
{
  "entity": {
    "user": "<wallet_login>",
    "ticket": "TGT-***"
  },
  "links": [
    {
      "rel": "sts",
      "href": "https://auth.qiwi.com/cas/sts"
    }
  ]
}
~~~

Тикет действует в течение пользовательской сессии. 

<aside class="notice">
Максимальное время жизни TGT-тикета составляет 8 часов. При отсутствии запросов st-ticket (см. ниже) сессия завершается через 2 часа.
</aside>

Для проверки наличия действующего TGT-ticket сессии можно использовать запрос:

`GET /cas/tgts`

~~~shell
user@server:~$ сurl "https://auth.qiwi.com/cas/tgts"
  -v --header "Accept: application/vnd.qiwi.sso-v1+json" 
  --header "Content-Type: application/json" 
~~~

~~~http
GET /cas/tgts HTTP/1.1
Host: auth.qiwi.com
User-Agent: ****
~~~

В ответе в Cookie `CASTGC` возвращается действующий TGT-тикет.

Для завершения действия тикета (завершения сессии) необходимо отправить запрос:

`DELETE /cas/tgts`

~~~shell
user@server:~$ сurl "https://auth.qiwi.com/cas/tgts"
  -X DELETE -v 
  -d '{"service":"https://qiwi.com/j_spring_cas_security_check,"ticket":"TGT-11223-***"}' 
  --header "Accept: application/vnd.qiwi.sso-v1+json" 
  --header "Content-Type: application/json" 
~~~

## Этап 2. st-ticket {#sts}

Для выполнения операций  **на каждый запрос к API** необходимо получать отдельный ST-ticket (service ticket) по действующему TGT-тикету.

~~~shell
user@server:~$ curl -X POST -v "https://auth.qiwi.com/cas/sts
  -d '{ "service":"https://qiwi.com/j_spring_cas_security_check","ticket":"TGT-***"}' 
  --header "Accept: application/vnd.qiwi.sso-v1+json" 
  --header "Content-Type: application/json" 
~~~

~~~http
POST /cas/sts HTTP/1.1
Accept: application/vnd.qiwi.sso-v1+json
Content-Type: application/json
Host: auth.qiwi.com
User-Agent: ****

{ "service":"https://qiwi.com/j_spring_cas_security_check","ticket":"TGT-***"}
~~~

Тип запроса - POST. 

URL запроса:

`https://auth.qiwi.com/cas/sts`

Специальные заголовки запроса:

* `Accept: application/vnd.qiwi.sso-v1+json`
* `Content-Type: application/json`

Тело запроса должно быть в формате JSON. Все перечисленные параметры обязательны: 

Параметр|Тип|Описание
--------|----|----
service | String |Идентификатор сервиса - строка `https://qiwi.com/j_spring_cas_security_check`
ticket|String| Полученный [TGT-ticket](#tgts)

ST-ticket содержится в поле `ticket` JSON-ответа.

~~~json
{
  "entity": {
    "ticket": "ST-***"
  },
  "links": []
}
~~~

## Этап 3. Cookie

<aside type="notice">Данный этап необходим только для выполнения платежных операций. Необходимо настроить на вашей стороне хранение файлов Cookie, т.к. по ним клиентская сессия идентифицируется при каждом запросе.</aside>

Для завершения авторизации необходимо отправить GET-запрос с указанием наименования сервиса и нового [ST-ticket](#sts).

~~~shell
user@server:~$ curl -v "https://qiwi.com/j_spring_cas_security_check?ticket=ST-22-***"
~~~

~~~http
GET /j_spring_cas_security_check?ticket=ST-22-1122 HTTP/1.1
Host: qiwi.com
User-Agent: ****
~~~

Тип запроса - GET. 

URL запроса:

`https://qiwi.com/j_spring_cas_security_check?ticket=<ticket>`

В строке запроса указывается полученный [ST-ticket](#sts)

Получение в ответе HTTP-кода 200 означает начало клиентской сессии, во время которой можно отправлять платежные запросы. Во время действия клиентской сессии получение дополнительных ST-ticket не требуется.

# Перевод на QIWI Кошелек {#p2p}

Тип запроса - POST.

URL запроса:

`https://qiwi.com/user/sinap/api/terms/99/payments/proxy.action`

~~~shell
user@server:~$ curl -X POST "https://qiwi.com/user/sinap/api/providers/99/onlineCommission/proxy.action"
  --header "Accept: application/vnd.qiwi.v2+json" 
  --header "Content-Type: application/json"
  --header "User-Agent: ***"
  -d '{"id":"11111111111111", "sum": {"amount":100, "currency":"643"}, "source": "account_643", 
       "paymentMethod": {"type":"Account", "accountId":"643"},
       "comment":"test", "fields": {"account":"9121112233"}}'
~~~

~~~http
POST /user/sinap/api/terms/99/payments/proxy.action HTTP/1.1
Accept: application/vnd.qiwi.v2+json
Content-Type: application/json
Host: qiwi.com
User-Agent: ****
 
{
	"id":"11111111111111",
	"sum": {
				"amount":100,
				"currency":"643"
			},
	"source": "account_643", 
	"paymentMethod": {
			"type":"Account",
			"accountId":"643"
			},
	"comment":"test",
	"fields": {
	 			"account":"9121112233"
	 		}
}
~~~

Авторизация выполняется по идентификатору сессии и сохраненному файлу Cookie на сервере.

<aside class="notice">
Для выполнения запроса необходимо отключить <a href="https://qiwi.com/settings/options/security.action">подтверждение платежей по SMS</a>.
</aside>

Специальные заголовки запроса:

* `Accept: application/vnd.qiwi.v2+json`
* `Content-Type: application/json`
 
Тело запроса - JSON. Все перечисленные параметры обязательны: 

Параметр|Тип|Описание
--------|----|----
id | String |Клиентский ID транзакции (максимум 20 цифр)
sum|Object| Объект, содержащий данные о сумме платежа: 
amount|Decimal|Сумма
currency|String|Валюта (только `643`, рубли)
source| String| Источник фондирования платежа. Допускается только следующее значение:<br>`account_643` - рублевый счет QIWI Кошелька отправителя
paymentMethod | Object| Объект, определяющий обработку платежа процессингом. Содержит следующие параметры:
type|String |Тип платежа, только `Account`
accountId|String| Идентификатор счета, только `643`.
comment| String| Комментарий к платежу.
fields|Object| Объект с параметрами перевода. Допускается только следующий параметр:
account| String|Номер телефона получателя (без международного префикса)


## Формат ответа

~~~json
{
    "id": ":11111111111111",
 	"fields": {
    	    "account": "9121112233"
  	},
  	"sum": {
    	   "amount": 100,
    	   "currency": "643"
  	},
  	"source": "account_643",
  	"transaction": {
    	   "id": "4969142201",
    	   "state": {
      		"code": "Accepted"
    	    }
  	}
}    
~~~

Успешный ответ содержит данные о принятом платеже:

Параметр | Описание
-----|--------
id | ID транзакции (максимум 20 цифр)
sum|Объект, содержащий данные о сумме платежа, из исходного запроса.
fields|Объект с параметрами перевода из исходного запроса.
source| Источник фондирования платежа из исходного запроса.
transaction|Объект с данными о транзакции в процессинге. Параметры:
id|ID транзакции в процессинге
state|Объект, содержит текущее состояние транзакции в процессинге. Содержит только параметр `code` со значением `Accepted` (платеж принят) 


# История платежей {#payments}

История платежей и пополнений вашего кошелька.

Для данного типа запросов необходимо получать специальный [ST-ticket](#sts) - в параметре `service` укажите `"service":"http://t.qiwi.com/j_spring_cas_security_check"`.

Тип запроса - GET.

~~~shell
user@server:~$ curl "https://edge.qiwi.com/payment-history/v1/persons/79112223344/payments?rows=10"
  --header "Accept: application/json" 
  --header "Content-Type: application/json"
  --header "Authorization: Token ST-***"
~~~

~~~http
GET /payment-history/v1/persons/79112223344/payments?rows=10&operation=OUT&sources[0]=QW_RUB&sources[1]=CARD HTTP/1.1
Accept: application/json
Authorization: Token ST-2724343-***
Content-type:application/json
Host: edge.qiwi.com
~~~

URL запроса:

`https://edge.qiwi.com/payment-history/v1/persons/<wallet>/payments?<parameters>`

Где:

* `<wallet>` - номер кошелька, для которого получена авторизация (с международным префиксом, но без `+`), **обязательный параметр**
* `<parameters>` - дополнительные параметры запроса (см. ниже)

**Авторизация запроса** выполняется по заголовку `Authorization`:

`Authorization: Token <ST-ticket>`

где `<ST-ticket>` - полученный в результате авторизации сессии ST-ticket.

Специальные заголовки запроса:

* `Accept: application/json`

Параметры запроса: 

Параметр|Тип|Описание|Обяз.
--------|----|----|------
rows | Integer |Число платежей в ответе, для разбивки отчета на части. Целое число от 1 до 50.|Да
operation|String| Тип операций в отчете, для отбора. Допустимые значения:<br>`ALL` - все операции, <br>`IN` - только пополнения, <br>`OUT` - только платежи, <br>`QIWI_CARD` - только платежи по картам QIWI (QVC, QVP). <br>По умолчанию `ALL`|Нет (если не указан, отбор не применяется)
sources|Array[String]|Источники платежа, для отбора. Каждый источник задается как отдельный параметр и нумеруется элементом массива, начиная с нуля (`sources[0]`, `sources[1]` и т.д.). Допустимые значения: <br>`QW_RUB` - рублевый счет кошелька, <br>`QW_KZT` - счет кошелька в тенге, <br>`QW_USD` - счет кошелька в долларах, <br>`CARD` - привязанные и непривязанные к кошельку банковские карты, <br>`MK` - счет мобильного оператора.|Нет (если не указан, отбор не применяется)
startDate | DateTime URL-encoded| Начальная дата поиска платежей (формат ГГГГ-ММ-ДДTчч:мм:ссTMZ). По умолчанию, равен вчерашней дате. Используется только вместе с `endDate`  | Нет
endDate | DateTime URL-encoded | Конечная дата поиска платежей (формат ГГГГ-ММ-ДДTчч:мм:ссTMZ). По умолчанию, равен текущей дате. Используется только вместе с `startDate` | Нет
nextTxnDate | DateTime URL-encoded| Дата транзакции (формат ГГГГ-ММ-ДДTчч:мм:ссTMZ), для отсчета от предыдущего списка (см. параметр `nextTxnDate` в ответе). Используется только вместе с `nextTxnId` | Нет
nextTxnId | Long | Номер предшествующей транзакции, для отсчета от предыдущего списка (см. параметр `nextTxnId` в ответе). Используется только вместе с `nextTxnDate`| Нет


<aside class="notice">Максимальный допустимый интервал между startDate и endDate - 90 календарных дней.</aside>

## Формат ответа

~~~json
{"data":
	[
		{
		"txnId":9309,
		"personId":79112223344,
		"date":"2017-01-21T11:41:07+03:00",
		"errorCode":0,
		"error":null,
		"status":"SUCCESS",
		"type":"OUT",
		"statusText":"Успешно",
		"trmTxnId":"1489826461807",
		"account":"0003***",
		"sum":{
				"amount":70,
				"currency":"RUB"
				},
		"commission":{
				"amount":0,
				"currency":"RUB"
				},
		"total":{
				"amount":70,
				"currency":"RUB"
				},
		"provider":{
                       "id":26476,
                       "shortName":"Yandex.Money",
                       "longName":"ООО НКО \"Яндекс.Деньги\"",
                       "logoUrl":"https://static.qiwi.com/img/providers/logoBig/26476_l.png",
                       "description":"Яндекс.Деньги",
                       "keys":"***",
                       "siteUrl":null
                      },
		"comment":null,
		"currencyRate":1,
		"extras":null,
		"chequeReady":true,
		"bankDocumentAvailable":false,
		"bankDocumentReady":false,
                "repeatPaymentEnabled":false
		}
	],
	"nextTxnId":9001,
	"nextTxnDate":"2017-01-31T15:24:10+03:00"
}
~~~

Успешный ответ содержит список платежей из истории кошелька, соответствующих заданному фильтру:

Параметр|Тип|Описание
--------|----|----
data|Array[Object]|Массив платежей. <br>Число платежей равно заданному параметру `rows`
txnId | Integer |ID транзакции в процессинге
personId|Integer|Номер кошелька
date|DateTime|Дата/время платежа (в формате ГГГГ-ММ-ДДTчч:мм:ссTMZ)
errorCode|Integer|Код ошибки платежа
error| String| Описание ошибки
status|String|Статус платежа. Возможные значения:<br>`WAITING` - платеж проводится, <br>`SUCCESS` - успешный платеж, <br>`ERROR` - ошибка платежа.
type | String| Тип платежа (соответствует параметру запроса `operation`)
statusText|String |Описание статуса платежа
trmTxnId|String|Клиентский ID транзакции
account| String|Номер счета получателя
sum|Object| Объект с суммой платежа. Параметры:
amount|Decimal|сумма, 
currency|String|валюта 
commission|Object| Объект с комиссией платежа. Параметры:
amount|Decimal|сумма, 
currency|String|валюта 
total|Object| Объект с общей суммой платежа. Параметры:
amount|Decimal|сумма, 
currency|String|валюта 
provider|Object| Объект с описанием провайдера. Параметры:
id|Integer|ID провайдера в процессинге,
shortName|String|краткое наименование провайдера,
longName|String|развернутое наименование провайдера,
logoUrl|String|ссылка на логотип провайдера,
description|String|описание провайдера (HTML),
keys|String|список ключевых слов,
siteUrl|String|сайт провайдера
comment|String|Комментарий к платежу
currencyRate|Decimal|Курс конвертации (если есть в транзакции)
extras|Object|Дополнительные поля платежа, список не фиксирован
chequeReady| Boolean|Специальное поле
bankDocumentAvailable|Boolean|Специальное поле
bankDocumentReady|Boolean|Специальное поле
repeatPaymentEnabled|Boolean|Специальное поле
nextTxnId|Integer|ID следующей транзакции в полном списке
nextTxnDate|DateTime|Дата/время следующей транзакции в полном списке

## Статистика платежей

Для получения статистики по суммам платежей за заданный период используется GET-запрос к API.

~~~shell
user@server:~$ curl "https://edge.qiwi.com/payment-history/v1/persons/79112223344/payments/total?startDate=2017-03-01T00%3A00%3A00%2B03%3A00"
  --header "Accept: application/json" 
  --header "Content-Type: application/json"
  --header "Authorization: Token ST-***"
~~~

~~~http
GET /payment-history/v1/persons/79112223344/payments/total?startDate=2017-03-01T00%3A00%3A00%2B03%3A00&endDate=2017-03-31T11%3A44%3A15%2B03%3A00 HTTP/1.1
Accept: application/json
Authorization: Token ST-2724343-***
Content-type:application/json
Host: edge.qiwi.com
~~~

URL запроса:

`https://edge.qiwi.com/payment-history/v1/persons/<wallet>/payments/total?<parameters>`

Параметры запроса:

* `<wallet>` - номер кошелька, для которого получена авторизация (с международным префиксом, но без знака "+"), **обязательный параметр**
* `<parameters>` - дополнительные параметры запроса (необязательные):

Параметр|Тип |Описание
--------|----|-------
startDate|DateTime | Начальная дата периода статистики (в формате ГГГГ-ММ-ДДTчч:мм:ссTMZ). **Если не указана, выгружаются данные с 1 числа текущего месяца.**
endDate|DateTime| Конечная дата периода статистики (в формате ГГГГ-ММ-ДДTчч:мм:ссTMZ).
operation|String| Тип операций, учитываемых в отчете. Допустимые значения:<br>`ALL` - все операции, <br>`IN` - только пополнения, <br>`OUT` - только платежи, <br>`QIWI_CARD` - только платежи по картам QIWI (QVC, QVP). <br>По умолчанию `ALL`.
sources|Array[String]|Источники платежа, учитываемые в отчете. Каждый источник задается как отдельный параметр и нумеруется элементом массива, начиная с нуля (`sources[0]`, `sources[1]` и т.д.). Допустимые значения: <br>`QW_RUB` - рублевый счет кошелька, <br>`QW_KZT` - счет кошелька в тенге, <br>`QW_USD` - счет кошелька в долларах, <br>`CARD` - привязанные и непривязанные к кошельку банковские карты, <br>`MK` - счет мобильного оператора. Если не указан, учитываются все источники платежа.

<aside class="notice">Максимальный период для выгрузки статистики - 90 календарных дней.</aside>

~~~json
{
 "incomingTotal":[
	{
	"amount":3500,
	"currency":"RUB"
	}],
 "outgoingTotal":[
 	{
 	"amount":3497.5,
 	"currency":"RUB"
 	}]
}
~~~

Успешный ответ содержит статистику платежей за соответствующий период:

Параметр|Тип|Описание
--------|----|----
incomingTotal|Array[Object]|Данные о входящих платежах (пополнениях), отдельно по каждой валюте
amount | Decimal |Сумма пополнений 
currency|String|Валюта пополнений
outgoingTotal|Array[Object]|Данные об исходящих платежах, отдельно по каждой валюте
amount | Decimal |Сумма платежей 
currency|String|Валюта платежей


# Баланс QIWI Кошелька

Для данного типа запросов необходимо получать специальный [ST-ticket](#sts) - в параметре `service` укажите `"service":"http://t.qiwi.com/j_spring_cas_security_check"`.

~~~shell
user@server:~$ curl "https://edge.qiwi.com/funding-sources/v1/accounts/79112223344
  --header "Accept: application/json" 
  --header "Content-Type: application/json"
  --header "Authorization: Token ST-***"
~~~

~~~http
GET /funding-sources/v1/accounts/79123332244 HTTP/1.1
Accept: application/json
Authorization: Token ST-2724343-***
Content-type:application/json
Host: edge.qiwi.com
~~~

Тип запроса - GET.

URL запроса:

`https://edge.qiwi.com/funding-sources/v1/accounts/<wallet>`

**Авторизация запроса** выполняется по заголовку `Authorization`. Указывается строка 

`Token <ST-ticket>`

где `<ST-ticket>` - полученный в результате авторизации сессии [st-ticket](#st).

Специальные заголовки запроса:

* `Accept: application/json`

Обязательный параметр запроса: 

Параметр|Тип|Описание
--------|----|----
wallet|Integer|Номер кошелька, для которого получена авторизация (с международным префиксом, но без `+`)

## Формат ответа

~~~json
{
  "accounts": [
    {
      "fsAlias": "qb_wallet",
      "title": "Qiwi Account",
      "type": {
        "id": "WALLET",
        "title": "Visa QIWI Wallet"
      },
      "currencyId": 643,
      "balance": {
        "amount": 3.15,
        "currencyId": 643
      }
    }
  ]
}
~~~

Успешный ответ содержит список источников фондирования платежей и балансы:

Параметр|Тип|Описание
--------|----|----
accounts|Array[Object]|Массив балансов
fsAlias | String |Название банковского баланса
title|String|Название счета кошелька
type|Object|Сведения о счете
id, title| String| Описание счета
currencyId | Integer| Код валюты счета (number-3 ISO-4217)
balance|Object |Сведения о балансе данного счета
amount|Decimal|Текущий баланс данного счета
currencyId | Integer| Код валюты счета (number-3 ISO-4217)

# Коды ошибок

В случае ошибки API возвращается HTTP-код ошибки.

Код | API | Описание
---|-----|---------
401 | Все | Ошибка авторизации
404 | История платежей | Не найдена транзакция или отсутствуют платежи с указанными признаками
404 | Балансы | Не найден кошелек
423 | История платежей | Слишком много запросов, сервис временно недоступен
